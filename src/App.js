import React, { useState, useEffect } from 'react';
import axios from 'axios';

function App() {
  const loaded = async () => {
    // fetch('http://localhost:3001/api/getData')
    let response = await axios.get('/api/getData');
    console.log(response.data);
    // if (response && response.ok) {
    //   let data = await response.json();
    //   // do something with data
    //   console.log(data);
    // }
  };
  useEffect(() => {
    loaded();
  });
  return (
    <div className='App'>
      <div className='text'>Haha</div>
    </div>
  );
}

export default App;
